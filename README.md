# Ansible role to test / demonstrate the use of Ansible-Vault
This role is a demo of a pattern to be used to protect secret variables of a role by encrypting them with Ansible-Vault.

## Procedure
Create the secret variable in ./defaults/main.yml file of your role and define it like a reference to another variable with the same name BUT prefixed by vault_

Exemple
```
---
my_variable: "hello world"
my_secret_variable: "{{ vault_my_secret_variable }}"
```

This enables you to see in one place all variables that can / should be defined for your role without compromising the secret. It also makes it very clear that these variables are encrypted using `ansible-vault`.

Now create a file `./vars/vault.yml`.  
2 possibilities here
1. Either create the file with the command `ansible-vault create vault.yml`  
This enables you to immediately create an encrypted file but it is only possible if you work on a Linux machine where ansible is installed.
2. Or create the file in your favorite code editor and encrypt it **BEFORE** committing it to your git repository !

Whatever method you choose, the encryption can only happen on a Linux machine where **Ansible** is available.

TODO: find a way to be warned when committing to git if a file is not encrypted ...

Here are the instructions to encrypt an existing file
```
ansible-vault encrypt vault.yml

New Vault password:
Confirm New Vault password:
Encryption successful

ls
vault.yml

cat vault.yml
$ANSIBLE_VAULT;1.1;AES256
38323334643632393464373461653036396565613966396665333437666337376331643062393063
6262336432313039656334346161303739333164383233330a646366346131623366303136386639
33623634393136316436323137646361353763633063643064356231666136343365663837393033
3365663838366334380a343839376232623765643732343130383337653464336664373464346563
61313630663334336162356561353337393035666261356338613862303463646339666234376662
66663433343136613634626339336439363138616138353238316262666161353130643066343534
376134333266306363316633643036336431
```

Now add a task in your role's task list to include the vault.yml file
```
# Include sensible data
- name: include encrypted vars
  include_vars: "vault.yml"
```
